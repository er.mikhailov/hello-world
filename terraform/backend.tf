terraform {
  backend "gcs" {
    bucket = "tf-state-helloworld"
    prefix = "terraform/state"
  }
}

data "terraform_remote_state" "helloworld" {
  backend = "gcs"
  config = {
    bucket = "tf-state-helloworld"

  }
}
