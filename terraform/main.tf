provider "google" {
  credentials = file(var.cred_file_path)
  project     = var.project
  region      = var.region
}


resource "google_container_cluster" "primary" {
  name                     = var.clustername
  project                  = var.project
  location                 = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  description              = "Initial Cluster"
}

resource "google_container_node_pool" "primary" {
  name       = "node-pool-primary"
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    labels = {
      type = "primary"
    }
    preemptible  = true
    machine_type = var.machinetype
  }
}

resource "google_container_node_pool" "monitoring" {
  name       = "node-pool-monitoring"
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    labels = {
      type = "monitoring"
    }
    preemptible  = true
    machine_type = var.machinetype
  }
  depends_on = [google_container_node_pool.primary]
}
