variable "region" {
  description = "GCP Region"
  default     = "europe-west4"
}

variable "zone" {
  description = "GCP Zone"
  default     = "europe-west4-a"
}

variable "project" {
  type        = string
  description = "GCP Project name"
}

variable "cred_file_path" {
  description = "PATH to credentials file"
}

variable "disk_image" {
  type        = string
  default     = "debian-10"
  description = "GCP Image Name for VM"
}

variable "clustername" {
  type        = string
  description = "Name of GKE Cluster"
}

variable "machinetype" {
  type        = string
  default     = "e2-small"
  description = "Type of created VM"
}
