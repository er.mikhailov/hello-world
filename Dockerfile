FROM python:alpine

# Make a working directory

WORKDIR /hello-world

#Let install Dependecies

COPY requirements.txt .
RUN pip install -r requirements.txt

# Copy source code

COPY . . 

#Assign tcp port to our project

EXPOSE 80/TCP

# Run code

CMD ["python", "src/main.py"]
