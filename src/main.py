from model import Currency, Settings
from flask import Flask, request, jsonify
import json
from requests import get
from decimal import Decimal
from prometheus_flask_exporter import PrometheusMetrics


# import time
import asyncio
import threading

sample = Flask(__name__)
metrics = PrometheusMetrics(sample)
settings = Settings()


async def get_currency_rates(URL):
    while True:
        global currency
        r = get(URL)
        external_data = json.loads(r.text)
        currency = Currency(**external_data)
        await asyncio.sleep(3600)


class Convert:
    def __init__(self, from_curr, to_curr, amount):
        self.from_curr = from_curr
        self.to_curr = to_curr
        self.amount = amount
        self.from_value = getattr(currency.rates, self.from_curr)
        self.to_value = getattr(currency.rates, self.to_curr)

    def calc(self):
        return self.to_value / self.from_value * Decimal(self.amount)


async def jsonify_convert_result(from_name, to_name, amount_from, amount_summ):
    response = {
        "request": {
            "from": from_name,
            "to": to_name,
            "amount": amount_from,
        },
        "response": {
            "amount": "{0:.2f}".format(amount_summ),
            "currency": to_name,
        },
    }
    return jsonify(response)


@sample.route("/")
def server():
    return "<b>hello world motherfucker!</b>"


@sample.route("/version")
def version():
    return "<b>Version v1.0.1 </b>"


@sample.route("/api/v1/currency")
async def currency_convert():
    data = Convert(
        request.args.get("from"),
        request.args.get("to"),
        request.args.get("amount"),
    )
    return await jsonify_convert_result(
        data.from_curr, data.to_curr, data.amount, data.calc()
    )


def flask_start():
    sample.run(host="0.0.0.0", port=80, threaded=True)


if __name__ == "__main__":
    threading.Thread(target=flask_start).start()

    main_loop = asyncio.get_event_loop()
    main_task = asyncio.run(get_currency_rates(settings.api_url))
    main_loop.run_until_complete(main_task)
    main_loop.close()
